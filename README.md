# Transporum

## Goal
To provide a modern internet forum system to protect freedom of speech in the era of governemnt suspression and bipolarization, with the following properties:
1. Full transparency
2. Planet-level scalbility
3. Optional annyomusity
4. Abuse detection and prevention
5. Highly mirrorable
6. Open core ecosystem

```plantuml
interface "JSON/Protobuf signed with user's private key and compressed" as SignedRequest
cloud {
    component ActionReceiver
    database Kafka
    component AdminService
    component BroadcastService
    component ContentService
    database Cassandra
    database "MongoDB/MySQL/PostgreSQL" as MainDB
    database "DB" as AdminDB
    database "Blob Store" as BroadcastS3
    component RankingService
    component AbuseDetector
    database Elasticsearch
    component "Frontend" as ContentFrontend
    database "Redis" as FrontendCache
}
node ExternalMirror

note top of ActionReceiver : Lightweight service that validates the user action request
SignedRequest --> ActionReceiver
ActionReceiver --> Kafka
Kafka --> AdminService
Kafka --> BroadcastService
Kafka --> ContentService
ContentService -- Cassandra : votes
ContentService -- MainDB : post / comments
AdminService -- AdminDB
BroadcastService --> BroadcastS3 : main server cross-signed action request
BroadcastS3 --> ExternalMirror
BroadcastService --> ExternalMirror : stream directly from kafka, also cross-signed by server
ExternalMirror --> ActionReceiver : allow external mirror providers to patch back their user actions
AdminService -- ActionReceiver : check user permission / banned users
Kafka --> RankingService
Kafka --> AbuseDetector
Kafka --> Elasticsearch
MainDB -- ContentFrontend
Cassandra -- ContentFrontend
FrontendCache -- ContentFrontend
ContentFrontend --> () "REST through gRPC gateway"
ContentFrontend --> () "GraphQL"
ContentFrontend --> () "gRPC"
```
